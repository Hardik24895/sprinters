package com.sprinters.utils

object Constant {
    const val SUBLIST = "sublist"
    const val TITLE = "title"
    const val MSG = "msg"
    const val ID = "id"
    const val DATA = "data"
    const val SKIP = "skip"
    const val YEAR = "year"
    const val ERROR = "error"
    const val UNAUTHORIZED = "unauthorized"
    const val COUNTRY_ID = "countryId"
    const val STATE_ID = "stateId"
    const val ENABLE = "visible"
    const val USER_ID = "userId"
    const val POSITION = "position"
    const val VIEW_POSITION = "view_position"
    const val HOME = "home"
    const val TOTAL_VOTE = "totalvotes"
    const val PROFILE_PIC = "profilePic"
    const val NAME = "name"
    const val FNAME = "fname"
    const val LNAME = "lname"
    const val COUNTRY = "country"
    const val STATE = "state"
    const val CITY = "city"
    const val URI = "uri"
    const val GLOBAL = "global"
    const val TYPE = "type"
    const val AS_HOST: String = "asHost"
    const val NOTIFICATION_TYPE = "notificationType"
    const val HOURS = "hours"
    const val LOGIN_COUNT = "logincount"
    const val MEDIA_ID = "mediaId"
    const val FILTER = "filter"
    const val PROFILE_PHOTO = "profilePhoto"
    const val COVER_PHOTO = "coverPhoto"
    const val BUCKET_NAME = "contestee"
    const val BUCKET_ACCESS_KEY = "AKIAJBM3XU4HNVVTAOXA"
    const val BUCKET_SECRETE_KEY = "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"


    //------User Profile Dialog Option---

    const val IMAGE = "image"
    const val VIDEO = "video"
    const val WATCH = "watch"
    const val WATCH_IMAGE = "watch_image"
    const val WATCH_ADD = "watch_add"


    //----Post Option-----

    const val LIKE = "Like"
    const val SAVE = "Save"
    const val SHARE = "Share"
    const val REPORT = "Report"
    const val DELETE = "Delete"
}