package com.contestee.utils

import android.content.ContentResolver
import android.content.Context
import android.content.res.Resources
import android.graphics.RectF
import android.net.Uri
import android.provider.MediaStore
import android.util.TypedValue
import android.view.View
import android.webkit.MimeTypeMap
import com.sprinters.utils.Logger
import java.net.URLConnection.guessContentTypeFromName
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log10
import kotlin.math.pow
import kotlin.math.roundToInt


object Utils {

    var lastClickTime: Long = 0
    val DOUBLE_CLICK_TIME_DELTA: Long = 500

    fun isDoubleClick(): Boolean {
        val clickTime = System.currentTimeMillis()
        if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
            lastClickTime = clickTime
            return true
        }
        lastClickTime = clickTime
        return false
    }

    fun calculateRectOnScreen(view: View): RectF {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return RectF(
            location[0].toFloat(),
            location[1].toFloat(),
            (location[0] + view.measuredWidth).toFloat(),
            (location[1] + view.measuredHeight).toFloat()
        )
    }

    fun calculateRectInWindow(view: View): RectF {
        val location = IntArray(2)
        view.getLocationInWindow(location)
        return RectF(
            location[0].toFloat(),
            location[1].toFloat(),
            (location[0] + view.measuredWidth).toFloat(),
            (location[1] + view.measuredHeight).toFloat()
        )
    }

    fun pxToDp(px: Float): Float {
        return px / Resources.getSystem().displayMetrics.density
    }

    fun dpToPx(dp: Float): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }

    fun convertDpToPixel(dp: Float, context: Context): Int {
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp,
            context.resources.displayMetrics
        )
        return px.roundToInt()
    }

    fun convertPixelToDp(px: Float, context: Context): Int {
        val dp = px / context.resources.displayMetrics.density
        return dp.roundToInt()
    }

    fun convertPixelToSp(px: Float, context: Context): Int {
        val sp = px / context.resources.displayMetrics.scaledDensity
        return sp.roundToInt()
    }

    fun getUriFromPath(context: Context, path: String): Uri {
        val res = context.resources
        return Uri.parse(
            "android.resource://"
                    + context.packageName + "/" + path
        )
    }

    fun toUri(context: Context, resourceId: Int): Uri {
        val res = context.resources
        return Uri.parse(
            ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + res.getResourcePackageName(resourceId)
                    + '/'.toString() + res.getResourceTypeName(resourceId) + '/'.toString() + res.getResourceEntryName(
                resourceId
            )
        )
    }

    fun getRealPathFromURI(context: Context?, uri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = context?.contentResolver?.query(uri, proj, null, null, null)
        if (cursor != null) {
            try {
                val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor.moveToFirst()
                return cursor.getString(columnIndex)
            } catch (ex: Exception) {
                ex.message?.let { Logger.e(it) }
            } finally {
                cursor.close()
            }
        }
        return null
    }

    fun getFileSize(size: Long): String {
        if (size <= 0) return "0"
        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (log10(size.toDouble()) / log10(1024.0)).toInt()

        return DecimalFormat("#,##0.#")
            .format(size / 1024.0.pow(digitGroups.toDouble())) + " " + units[digitGroups]
    }

    fun getSize(size: Double): Int {
        if (size <= 0) return 0
        val digitGroups = (log10(size) / log10(1024.0)).toInt()
        return (size / 1024.0.pow(digitGroups.toDouble())).roundToInt()
    }

    fun isImageFile(path: String): Boolean {
        val mimeType = guessContentTypeFromName(path)
        return mimeType != null && mimeType.startsWith("image")
    }

    fun isVideoFile(path: String): Boolean {
        val mimeType = guessContentTypeFromName(path)
        return mimeType != null && mimeType.startsWith("video")
    }

    fun getMimeType(url: String?): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return type
    }
    fun getOneMonthAgoDate(): String {
        val calendar: Calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -1)
        val date: Date = calendar.time
        val format = SimpleDateFormat("MMM", Locale.US)
        return format.format(date) + calendar.get(Calendar.YEAR)
    }
}
