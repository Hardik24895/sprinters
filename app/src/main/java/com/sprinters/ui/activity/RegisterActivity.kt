package com.sprinters.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.contestee.extention.goToActivityAndClearTask
import com.contestee.extention.onBackPress
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sprinters.R
import com.sprinters.ui.activity.bottomMenu.HomeActivity

import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.imageView2
import kotlinx.android.synthetic.main.activity_register.textInputLayout


class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
         applyAnimation()
        btnSignUp.setOnClickListener { goToActivityAndClearTask<HomeActivity>() }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onBackPress<RegisterActivity>()
    }
    fun  applyAnimation(){
        YoYo.with(Techniques.Shake)
            .duration(2000)
            .repeat(0)
            .playOn(imageView2);

        YoYo.with(Techniques.Landing)
            .duration(2000)
            .repeat(0)
            .playOn(textInputLayout);

        YoYo.with(Techniques.ZoomIn)
            .duration(2000)
            .repeat(0)
            .playOn(btnSignUp);
    }
}