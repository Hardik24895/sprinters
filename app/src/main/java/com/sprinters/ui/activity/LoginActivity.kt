package com.sprinters.ui.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.contestee.extention.goToActivityAndClearTask
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sprinters.R
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        applyAnimation()
        btnSignIn.isEnabled = false
        edtPhone.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                if (s != "" && s.length>=10) {
                    btnSignIn.isEnabled = true
                }else{
                    btnSignIn.isEnabled = false
                }
            }
            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })

        btnSignIn.setOnClickListener { goToActivityAndClearTask<OtpActivity>() }


    }

    fun  applyAnimation(){
        YoYo.with(Techniques.Shake)
            .duration(2000)
            .repeat(0)
            .playOn(imageView2);

        YoYo.with(Techniques.Landing)
            .duration(2000)
            .repeat(0)
            .playOn(textInputLayout);

        YoYo.with(Techniques.ZoomIn)
            .duration(2000)
            .repeat(0)
            .playOn(btnSignIn);
    }
}