package com.sprinters.ui.activity.bottomMenu.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sprinters.R
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_profile, container, false)
      //  activity?.overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_bottom);

        applyAnimation(root)

        return root
    }

    fun  applyAnimation(view: View){
        YoYo.with(Techniques.ZoomIn)
            .duration(1000)
            .repeat(0)
            .playOn(view.findViewById(R.id.imgProfile));

        YoYo.with(Techniques.Bounce)
            .duration(2000)
            .repeat(0)
            .playOn(view.findViewById(R.id.btnMyEvent));

        YoYo.with(Techniques.Landing)
            .duration(1000)
            .repeat(0)
            .playOn(view.findViewById(R.id.card1));
        YoYo.with(Techniques.Landing)
            .duration(1500)
            .repeat(0)
            .playOn(view.findViewById(R.id.card2));
        YoYo.with(Techniques.Landing)
            .duration(2000)
            .repeat(0)
            .playOn(view.findViewById(R.id.card3));
    }
}