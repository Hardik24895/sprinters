package com.sprinters.ui.activity

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.contestee.extention.goToActivityAndClearTask
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sprinters.R
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_splash.*
class SplashActivity :AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        applyAnimation()
        Handler().postDelayed({
            validateRedirection()
        }, 2500)
      /*  Glide.with(this).load(R.drawable.splashgif)
            .into(imageView)*/

    }

    private fun validateRedirection() {
        goToActivityAndClearTask<LoginActivity>()
    }
    fun  applyAnimation() {
        YoYo.with(Techniques.Wobble)
            .duration(2500)
            .repeat(0)
            .playOn(imageView);
    }
}