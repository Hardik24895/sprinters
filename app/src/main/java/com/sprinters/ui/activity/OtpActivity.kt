package com.sprinters.ui.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.contestee.extention.getValue
import com.contestee.extention.goToActivityAndClearTask
import com.contestee.extention.onBackPress
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.sprinters.R
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*


class OtpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        txtTitle.text = "Verification Code"
        pinView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {

                if (s.length == 4) {
                    goToActivityAndClearTask<RegisterActivity>()
                }
                Log.d(
                    "",
                    "onTextChanged() called with: s = [$s], start = [$start], before = [$before], count = [$count]"
                )
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        applyAnimation()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onBackPress<OtpActivity>()
    }

    fun  applyAnimation() {
        YoYo.with(Techniques.Landing)
            .duration(2000)
            .repeat(0)
            .playOn(root);
    }
}