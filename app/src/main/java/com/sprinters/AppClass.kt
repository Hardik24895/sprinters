package com.sprinters

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner

import com.contestee.utils.SessionManager

import com.sprinters.utils.Logger



class AppClass : Application(), LifecycleObserver {
    private var session: SessionManager? = null

    override fun onCreate() {
        super.onCreate()

        session = SessionManager(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)

        /*if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.api_key), Locale.getDefault())
        }*/
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        Logger.e("Application status: onAppBackgrounded")
        if (session != null && session?.isLoggedIn!!) {
            val editor = getSharedPreferences("Session", Context.MODE_PRIVATE).edit()
            editor.putLong("OutTime", System.currentTimeMillis())
            editor.apply()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        Logger.e("Application status: onAppForegrounded")

        if (session != null && session?.isLoggedIn!!) {
            val prefs = getSharedPreferences("Session", Context.MODE_PRIVATE)
            val minsIn = prefs.getLong("InTime", 0)
            val minsOut = prefs.getLong("OutTime", 0)

            val millis = minsOut - minsIn
            val minutes = (millis / 1000) / 60

            Logger.e("App Usage : $minutes min")
            //API call for update time on server
            if (minutes > 0) {
               // appUsage(minutes)
            }

            val editor = getSharedPreferences("Session", Context.MODE_PRIVATE).edit()
            editor.putLong("InTime", System.currentTimeMillis())
            editor.apply()
        }
    }

    /*private fun appUsage(value: Long) {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL

        val deviceInfo = "Android, Manufacturer $manufacturer  model $model"

        val params = HashMap<String, Any>()
        params["deviceType"] = deviceInfo
        params["minutes"] = value

        Networking
            .with(this)
            .getServices()
            .loginCount(Networking.wrapParams(params))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CallbackObserver<GenericModel>() {
                override fun onSuccess(response: GenericModel) {

                }

                override fun onFailed(code: Int, message: String) {

                }

            })
    }

    public fun logEvent(name: String) {
        if (session?.isLoggedIn!!) {
            val params = HashMap<String, Any>()
            params["pageName"] = name
            //params["userCountry"] = country.toString()

            Networking
                .with(this)
                .getServices()
                .pageViewed(Networking.wrapParams(params))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CallbackObserver<GenericModel>() {
                    override fun onSuccess(response: GenericModel) {

                    }

                    override fun onFailed(code: Int, message: String) {

                    }

                })
        }
    }*/
}